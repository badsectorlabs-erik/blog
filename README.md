# Bad Sector Labs Blog

This blog is a static site built with [Pelican](https://blog.getpelican.com/) and based on the 
[m.css](http://mcss.mosra.cz/) theme.

For more details, read 
[this blog post](https://blog.badsectorlabs.com/pelican-gitlab-cicd-docker-aws-awesome-static-site.html).

The DIY Guide to Forming an Online Business with Multiple Members in Different States
#######################################################################################

:date: 2018-05-6 11:40
:category: Business
:tags: Business
:summary: We navigate the surprisingly difficult world of business formation so you don't have to

.. role:: raw-html(raw)
   :format: html

.. role:: strike
    :class: strike

.. block-success:: TLDR

    We thought starting a simple two member business for our various revenue generating online endeavours would be straight forward
    - we were wrong. We couldn't find a good resources online so we slogged through legal, tax, and other formation issues and decided the best path for us
    was to set up a member-managed Delaware LLC that will be taxed as partnership. All for $248. Hopefully our pain
    will be helpful for others in a similar situation.

.. note-warning:: Disclaimer

    Like most things, we took a DIY approach to forming a business (we thought ‘how hard can this be!?’)
    and were only looking to set up a business correctly and cost-efficiently. We did not wish to pay lawyers,
    accountants, CPAs and others if we could avoid it. We did have initial (free) conversations with both a lawyer
    and tax professional. Everything below is based on what we did and should be used at your own peril.
    YMMV. And if we have got anything incorrect or mixed up, do let us know.

Background
==========

We wanted to set up a simple, legitimate business without spending a lot of money. One of our initial side projects
(`Hamiltix.net <https://www.hamiltix.net>`_) was generating a small amount of revenue and with other small
potential revenue generating projects in the pipeline, we needed a way to separate the finances and
professionally interact with other businesses (for example, we would eventually like to publish apps in the Apple App
Store / Google Play Store under our company name instead of using our names).

It seems like startups abound these days, so we would have thought the actual process for setting up a business would have
been more straightforward. In our case, we needed to set up a business with two owners who reside in different
US states, with no employees, where the business would be entirely online with no physical presence or store front,
and we would be working on business related projects from our respective homes.

Given the lack of resources online on the specific mechanics of actually forming a business with this profile,
we wanted to write down the process we went through in hopes that it will be helpful to others in a similar situation.
We have complied our notes on this process and laid out the decision tree and steps we went through. The below is an
amalgamation of hours of internet research (which almost always ended up with something like `this
<https://www.quora.com/My-company-is-incorporated-in-Delaware-If-I-use-my-home-address-in-Illinois-as-the-physical-address-to-get-Federal-EIN-will-there-be-any-negative-implications>`_

 *"better check with your lawyer."*


Step 1
=========
Determine Which Type of Business You Are / Are Going to Be / Want to Be
------------------------------------------------------------------------

* If you have some or all of the below characteristics, a Delaware C Corporation is likely the way to go:
    * Expect to raise money from outside parties such as venture capital firms or sell your business to a larger company
    * Expect to hire employees and incent them with ownership in your company
    * The easiest, cheapest, and best way to go seems to be to use `Clerky <https://www.clerky.com/>`_ or `Stripe Atlas <https://stripe.com/atlas>`_ and pay the ~$500+ and just be done with it (note we haven’t used these services or tried them, this is solely based on our research. However, Y Combinator backed and uses Clerky for its companies). We won’t spend any more time detailing this path as this does not describe us
* If you align more with the characteristics below you may want to create a Limited Liability Company (LLC) or S Corporation
    * Expect to be profitable in the near term
    * Have one founder or a limited number of founders / investors who are mostly friends or family `Stripe <https://stripe.com/atlas/guides/llc-vs-c-corp#where-is-the-llc-formed>`_ does a good job of explaining the difference of LLCs and C Corporations in more detail.

For LLCs and S Corporations, profits or losses are `passed through <https://www.thebalancesmb.com/pass-through-taxes-and-business-owners-398390>`_ to the individual owners based on their underlying
ownership interests in the company and then each owner is taxed on those earnings (or losses) based on their individual
tax rates. For example, if the business revenue was $10, business costs were $5, then $5 dollars of profit would be split
between the owners by their ownership amounts. An owner's allocated amount would then be included on their individual
tax return.

For a C Corporation, the company itself would pay taxes on any profits or losses, and then if the owners wanted to
receive cash from the company, the company would have to pay a dividend to its owners. A C Corporation owner would
then have to pay tax on the dividend. This leads to earnings being taxed twice - once at the C Corporation and again to
the investor on their dividends. There is also a board of directors structure in a C corporation which differs from a
LLC which generally has less requirements.

Ultimately, a business is just an entity, like a person, that can open accounts, take on debt, and interact with
other companies. But one nice reason for setting up a business (and doing it correctly) is that it provides some liability
protection to the owners and cleanly separates business finances from personal finances.

One more quick note on proper terminology related to LLCs.
In an LLC you own "units" and are a "member" versus a corporation where
you own shares of stock and are a shareholder or a stockholder. As a shareholder in a corporation you also then have the
right to vote your shares to elect a board of directors who then hire management and guide the corporation where the LLC
requires less formalities.

Step 2
=========
Where to Form Your Business?
------------------------------
Once you have decided which type of entity you will be forming you now have to decide where to incorporate. You can
incorporate a business in any state, even if you do not live, work, or have an office there. If you are a sole member,
or have members in the same state, it may make sense to form the business in your home state.

In our case however, there are two of us located in different states and we were planning on operating an online
business with no physical presence. As a result, we decided to form our business in Delaware (even though neither of us
live in Delaware). The reason why Delaware is one of the most popular choices for registering a business is because
it has significant historical business legal case law (which makes investors, lawyers, etc. more comfortable that they
know how certain legal situations will unfold). Delaware charges a $90 formation fee (for an LLC) and has a $300 annual
“tax” beginning at the end of year 1 and each year thereafter to maintain registration. Different states have different
annual payment requirements (California being one of the most expensive).

Finally there is the potential to have to register in multiple states if you are "doing business" or have "tax nexus" in
a state other than the state in which you formed. We haven't been able to find a good answer on what the definition of
"doing business" is just yet, but in our situation when we had our initial conversation with the tax accountant our
arrangement of filling in Delaware only for now seemed appropriate. We think that since we are an online business,
and do not have a physical store front or office, or have physical product at any time (software only), we do not need
to register in other states, but check back with us next April on that one... It would seem a bit extreme (and
prohibitively expensive) for us to have to register in all 50 states, for example, just because we might sell to a
customer in one of those states. I can't say we fully understand this yet but the tax accountants were ok with only a
Delaware registration. If you have better information or understanding on this point and can tell us what
"doing business" actually means please let us know! For reference, if we did have to register our Delaware LLC in
another state it is known as a "foreign" registration.

Step 3
=======
Name Picked? Good, It's Formation Time
---------------------------------------

Once you have chosen a state to form your business you will need to determine who will be the registered agent.
All business must have a registered agent whose address and information are
`publicly available <https://icis.corp.delaware.gov/Ecorp/EntitySearch/NameSearch.aspx>`_
and is the location where the business is to receive process of service (court documents if the business is sued).
You can be your own registered agent (if you have an address in the state you a registering) but then your information
(name and address) are in the public record. This is solved by utilizing a paid registered agent service.
The paid registered agent service basically serves two purposes:

1. Be the name and address on the publicly available information on your business so you do not have to put your personal information, and
2. Have an online document portal where you will get alerts any time documents are added (so if you were to get sued, the court documents would go to the registered agent's address, and the registered agent would then scan the documents and upload it to the portal for you to view

There are a lot of registered agent services out there but we decided to go with `A Registered Agent Inc (Delaware Registered Agent) <https://www.delawareregisteredagent.com/>`_
which after researching a bunch of options seemed to be on the cheaper end of the spectrum and didn't aggressively try to
upsell you on every little addition. Unless you absolutely need it, do not pay the extra $50 to expedite your
formation. We were formed and had our documents back from Delaware Registered Agent in seven days. They charge $180
for the registration (which would cost $90 if you just did it yourself) and the remainder covers preparation costs, the
first year of registered agent services, initial LLC resolutions, and a form LLC operating agreement. Delaware Registered
Agent charges $45 each year thereafter for its registered agent services (to use their address and have them scan
legal documents for you in the event of legal action).

Now if only there were a decentralized, distributed ledger system for keeping track of business formations and
business ownership...


Step 4
=======
Get Squared Away with the IRS
------------------------------

After you have formed the business the next step is to get squared away with the IRS - that means you need to get an
`Employer Identification Number <https://www.irs.gov/businesses/small-businesses-self-employed/apply-for-an-employer-identification-number-ein-online>`_
("EIN") (basically a social security number for your business). The online portal is the easiest way to do this and it
takes about 10-15 minutes. Strangely the online portal is only available Monday through Friday from 7AM to 10PM - last
I checked servers do not need to sleep, so if anyone knows why this is the case we are genuinely curious to know.

The online portal is the equivalent of filling out a form `SS-4 <https://www.irs.gov/pub/irs-pdf/fss4.pdf>`_ on paper
and mailing it in, except the online portal takes 10 minutes and you get your EIN number right away. The key items when
filling out the form online are the "Type of Entity" and the employment tax liability checkbox. While we were very tempted to
register as a "church or church controlled organization"
(which enjoy some pretty nice `tax exemptions <https://youtu.be/7y1xJAVZxXg?t=8m16s>`_), we went with the default for a
multi-member LLC, a partnership. The thing to understand here is now you have an LLC
that will be taxed as a partnership for tax purposes. This is what we (and probably you if you're in a similar
situation) wanted as this allows us to pass through any profits to each of the members' annual tax fillings
(and avoid double taxation of a C corporation). We also do not plan to have any employees - it will just be us (the owners / members) running the business - and as a result we made sure to check the employment tax liability checkbox (item 14
on the paper SS-4), we only will need to make one annual employer federal tax return filing on `form 944 <https://www.irs.gov/pub/irs-pdf/f944.pdf>`_
versus quarterly federal tax returns on `form 941 <https://www.irs.gov/pub/irs-pdf/f941.pdf>`_. If you were to have
employees you'll have to make sure you are withholding the appropriate amount from their paychecks for taxes and file the quarterly form.

LLC Tax Responsibilities
----------------------------
Below we have compiled a summary of the tax forms the LLC will need to file:

* LLC files Form 1065 annually with the IRS - this is just an “FYI” to the IRS by March 15th (since the LLC doesn't pay taxes itself as it is a pass through entity)
* LLC files Form 944 annually with IRS (to file form 944 annually vs. form 941 quarterly you must (i) have less than $1k of wages paid to employees per year, and (ii) check box number 14 on form SS-4 (or the equivalent on the online EIN portal) when filing to get your EIN)
* LLC prepares and provides `K-1 statements Form 1041) <https://www.irs.gov/pub/irs-pdf/f1041sk1.pdf>`_ to its members by March 15th

Member Tax Responsibilities
------------------------------
* Member's will be required to file and pay estimated taxes on `1040-ES <https://www.irs.gov/pub/irs-pdf/f1040es.pdf>`_ a quarterly basis if:

 *"You expect to owe at least $1,000 in tax for 2018, after subtracting your withholding and refundable credits."*

* Members use K-1 (Form 1041) statements to complete their 1040 and Schedule E by April 15th

.. note-danger:: Note on Member Taxes

    Note that members will pay taxes on their share of the LLC's profits *regardless of if the LLC distributes the profits or not*.
    The LLC of course can make distributions in accordance with the LLC operating agreement (see below), which depending
    on how the operating agreement is drafted could be anytime. Our operating agreement is drafted such that distributions
    can be made to the members anytime by unanimous consent (remember the distributions must
    be in proportion to the ownership).

Step 5
=======
LLC Operating Agreement
------------------------------
For an LLC, the operating agreement is a document that outlines how the business will operate. It typically includes things
like management of the business, ownership, distributions, buyout of other members, disputes, etc. We took the
template provided by our registered agent (similar template `here <https://www.northwestregisteredagent.com/pdf/operating-agreement.pdf>`_) and modified it to our liking and went with that.

The operating agreement is not filed with any government institution or bureau and is signed and kept between members,
or if you have a lawyer, the lawyer will usually keep a copy of the documents as well. For reference, we talked to a
small business lawyer who offered to write a simple operating agreement for us for $1,500.
Perhaps if we start bringing in larger amounts of money at some point in the future we will update the agreement,
but for now the modified template works just fine for us. We have some experience and familiarity dealing
with operating agreements, but even if you do not, the templates we looked at is probably adequate for
just starting out.

You can amend the operating agreement at any time (and depending on how you worded your initial operating agreement
you may require unanimous or majority approval of unit holders to amend the operating agreement).


Step 6
=======
The Money
------------------------------
Now that you are all set on taxes, you should set up a business checking account to accept payments and pay any
*business* expenses such as `server costs <https://blog.badsectorlabs.com/how-we-built-hamiltixnet-for-less-than-1-a-month-on-aws.html>`_.
Setting up a checking account also proved to be unnecessarily difficult. We wanted to go with a large national bank
that was present in both of our home states (recall we are two members in different states) so we tried to get an
account at Chase. However after calling and visiting multiple chase branches, we found that they have a policy that
requires a business account to be registered in the state in which the branch you are opening the account is located.
Since our business was registered in Delaware, but neither of us reside in Delaware and the Chase branches
we have access to are not in Delaware, we could not open a business checking account with Chase.

We then tried another national bank and after walking into a branch with all documents from the steps above
in hand (formation document, initial resolutions, EIN letter, operating agreement) they weren't sure either if they
could open an account for a Delaware LLC since the branch was not in Delaware. They copied our LLC Operating Agreement,
formation document, and initial resolutions and said they would check with someone outside the branch and get back to
us. The next day they called back saying they could open the account. We did get the account opened but only I was listed
on the account since they required my business partner (who is in a different state) to be physically
present with me in order for him to be added to the account. Fortunately, he was planning to be in town in a few weeks
but this is another hurdle to be aware of. Also, apparently there is such a difference between a personal checking
account and a business checking account that it requires a "business banker", whatever that is. There was one occasion
were we visited a branch on a weekend only to have no "business banker" available.

Business checking accounts all also somewhat different personal checking accounts. One difference is transaction volume
and size. Our account only allows 150 transactions a month (with $0.50 charge for any transactions thereafter).
Additionally, our account requires a minimum $1,500 balance in order to avoid a $12 monthly charge. Lastly, if you are
working with a lot of cash for some reason (which we of course are not as everything is online) there are maximums and
other fees related to cash deposits. So be aware of all the limitations, which are more stringent than personal
checking accounts and to us just feel like ways for banks to charge additional little fees because the account is a
"business" account. We looked at a bunch of different banks and all have similar types and amount of fees.

Our bank also required us to sign a banking resolution. This is just a legal document signed by the LLC members that
officially gives the members authority to open an account for the LLC.

Lastly, now that we have an account set up we need to track the money and each member's capital accounts (remember for
taxes in the next year, you will need to fill forms that show how much money the LLC made and how it is allocated to
its members). We haven't figured out the optimal solution here yet, but it seems `Wave <https://www.waveapps.com/>`_
might be a good free option based on research (vs. paying monthly fees for some other services).

Miscellaneous
==============
* **Insurance**: Getting insurance for your LLC is probably a good idea and depending on your business there might be certain coverages that are better for you. Depending on what you get, for a small business it should not be any more than a couple hundred dollars.
* **Maximizing the Liability Protection Benefits of an LLC**: In order to maximize the potential benefits of the "limited liability" portion of an LLC you will want to make sure you *do not* use the LLC for any personal expenses or other matters and solely use it for business purposes (hence one reason why we set up a separate business checking account in Step 6 above).

Conclusion
===========
All told we spent what seemed like way too long to set up a business that is relatively common and straightforward.
Fortunately, going the DIY route we only ended up spending $248 in total ($180 for A Registered Agent to do the
registration and provide registered agent services and another $68 for 6 months of a USPS PO Box to get any physical
mail we might receive), excluding any insurance.


Our Projects
====================
`Hamiltix.net <https://www.hamiltix.net>`_: We use algorithms to rank the best available Hamilton tickets
across all available tickets and dates based on various factors, and allow users to search across any
combination of dates, sections, and prices in addition to setting up email alerts.

FirstUp Fitness: Getting up at 4:45 AM to make tomorrow's spin class reservation? Refreshing the page to reserve a class as soon
as it opens up for reservations? Our app will let automatically reserve a spot for a fitness class as soon as it opens
for reservations. You just set the reservation you want and forget it - FirstUp Fitness will do the reserving for you.
If you belong to an Equinox and this sounds like something you would use - drop us a line. We are currently
developing the app.

Questions or comments?
blog (at) badsectorlabs.com

#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Erik'
SITENAME = 'Bad Sector Labs Blog'
SITEURL = ''

PATH = 'content'

STATIC_PATHS = [
    'images', 
    'extra/robots.txt', 
    'extra/favicon.ico'
]
EXTRA_PATH_METADATA = {
    'extra/robots.txt': {'path': 'robots.txt'},
    'extra/favicon.ico': {'path': 'favicon.ico'}
}

SUMMARY_MAX_LENGTH = 50

TIMEZONE = 'America/New_York'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None


# Social widget
SOCIAL = (('twitter', 'https://twitter.com/badsectorlabs'),
          ('gitlab', 'https://gitlab.com/badsectorlabs'))

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'm.css/pelican-theme'
THEME_STATIC_DIR = 'static'
DIRECT_TEMPLATES = ['index']

M_CSS_FILES = ['https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i%7CSource+Code+Pro:400,400i,600',
                       '/static/m-dark.css']
M_THEME_COLOR = '#22272e'

PLUGIN_PATHS = ['m.css/pelican-plugins']
PLUGINS = ['m.htmlsanity', 'm.code', 'm.components', 'm.images']

M_IMAGES_REQUIRE_ALT_TEXT = False

M_FAVICON = ('favicon.ico', 'image/x-ico')
M_SITE_LOGO = 'images/logo.png'
M_SITE_LOGO_TEXT = ' '

# Social
M_BLOG_NAME = "Bad Sector Labs Blog"
M_BLOG_URL = 'https://blog.badsectorlabs.com/'
M_BLOG_DESCRIPTION = "A Blog about DevOps, infrastructure, Python, exploit development, and more!"

M_SOCIAL_TWITTER_SITE = '@badsectorlabs'
M_SOCIAL_TWITTER_SITE_ID = 416718790
M_SOCIAL_IMAGE = 'https://blog.badsectorlabs.com/images/social.jpg'
M_SOCIAL_BLOG_SUMMARY = "A Blog about DevOps, infrastructure, Python, exploit development, and more!"

M_COLLAPSE_FIRST_ARTICLE = False

RAW_FOOTER = SITENAME + ''' &copy <script type="text/javascript">document.write(new Date().getFullYear());</script>
| Powered by <a href="https://getpelican.com" target="_">Pelican</a> 
and <a href="http://mcss.mosra.cz" target="_">m.css</a> (customized)'''
